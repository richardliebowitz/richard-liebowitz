Richard Liebowitz
Richard Liebowitz is an attorney and photographer based in New York.

His passion is for protecting the rights of photographers whose work is taken and used without compensation. Richard Liebowitz works hard to fight for those who do not have a voice against the large media companies. Contact Richard with questions.

Richard Liebowitz is a copyright lawyer and avid photographer, advocating for the rights of photographers.

Website : https://richardliebowitz.com/